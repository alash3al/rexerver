<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DomainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domains = collect(json_decode(\App\Helper\Guzzle::getClient()->get("/zone/all")->getBody()->getContents())->data)->map(function($domain) {
            $domain->id = $domain->name;
            return $domain;
        });
        return view('content.domain-list', compact('domains'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ( $validator->fails() ) {
            return redirect()->back()->with('alert', $validator->errors()->first())->with('alert-class', 'alert-danger');
        }

		try {
			$found = (bool) (json_decode(\App\Helper\Guzzle::getClient()->get("/zone/{$request['name']}")->getBody()->getContents())->data);
		} catch (\Exception $e) {
			$found = false;
		}

		if ( $found ) {
			return redirect()->back()->with('alert', 'The domain you tried to add already exists!')->with('alert-class', 'alert-warning');
		}

        $body = [
            'name' => $request['name'],
            'hits' => 0,
            'records' => []
        ];

        $domain = json_decode(\App\Helper\Guzzle::getClient()->post("/zone", ['json' => $body])->getBody()->getContents())->data;

        return redirect()->route('domain.edit', $request['name'])->with('alert', 'Domain has been created successfully')->with('alert-class', 'alert-success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $domain)
    {
        if ( isset($request['purge']) ) {
            \App\Helper\Guzzle::getClient()->delete("/zone/{$domain}");
            return redirect()->route('domain.index')->with('alert', "{$domain} has been deleted!");
        }

        $domain = (json_decode(\App\Helper\Guzzle::getClient()->get("/zone/{$domain}")->getBody()->getContents())->data);
        $domain->id = $domain->name;
		$id = 0;
        $domain->records = collect($domain->records)->map(function($record) use($domain, &$id) {
			++ $id;
			$record->id = $id;
			$record->type = intval($record->type);
            return (array) $record;
        })->prepend([
            'id' => 0,
            'type' => -1,
            'name' => '',
            'ttl' => 3600,
            'value' => ''
        ]);

        return view('content.domain-manage', compact('domain'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $domain)
    {
        $records = $request->get('records');
        $zone = json_decode(\App\Helper\Guzzle::getClient()->get("/zone/{$domain}")->getBody()->getContents())->data;

        if ( empty($records[0]['type']) ) {
            unset($records[0]);
        }

        \App\Helper\Guzzle::getClient()->post("/zone", ['json' => [
            'name' => $domain,
            'hits' => (int) $zone->hits,
            'records' => collect(array_values($records))->map(function($record){
                $record['ttl'] = intval($record['ttl']);
                $record['type'] = intval($record['type']);
                return $record;
            })->toArray()
        ]]);

        return redirect()->back()->with('alert', 'Domain records have been updated')->with('alert-class', 'alert-success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
