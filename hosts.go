package main

import "os"
import "sync"
import "bufio"
import "regexp"
import "strings"

type Hosts struct {
	store *sync.Map
}

var (
	HostsFiles *Hosts
)

func InitHosts() {
	HostsFiles = new(Hosts)
	HostsFiles.store = new(sync.Map)
	HostsFiles.ScanAndLoad()
}

func (this Hosts) Find(name string) (ip string) {
	val, ok := this.store.Load(name)
	if !ok {
		return ""
	}
	return strings.TrimSpace(val.(string))
}

func (this Hosts) ScanAndLoad() {
	for _, filename := range strings.Split(*HOSTS, ",") {
		file, err := os.Open(filename)
		if err != nil {
			continue
		}
		defer file.Close()
		r := bufio.NewReader(file)
		line, err := r.ReadString('\n')
		for err == nil {
			line = string(regexp.MustCompile(`\s+`).ReplaceAll([]byte(strings.TrimSpace(line)), []byte(" ")))
			if line != "" && !strings.HasPrefix(line, "#") {
				parts := strings.Split(line, " ")
				if len(parts) > 1 {
					for _, v := range parts[1:] {
						this.store.Store(v, parts[0])
					}
				}
			}
			line, err = r.ReadString('\n')
		}
	}
}
