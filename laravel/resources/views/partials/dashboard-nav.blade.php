<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{config('app.url')}}">{{config('app.name')}}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
	      <li class="dropdown"><a href="{{route('domain.index')}}"><i class="fa fa-globe"></i> Domains</a></li>
	      <li class="dropdown"><a href="{{env('SYSINFO_URL') ?: 'javascript:alert("Please configure it!")'}}"><i class="fa fa-info"></i> SysInfo</a></li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="fa fa-users"></span> Managers <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{route('user.index')}}">View All</a></li>
                <li><a href="{{route('user.create')}}">Create New</a></li>
              </ul>
            </li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
