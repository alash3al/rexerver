package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func APIServerInit(addr string) error {
	e := echo.New()
	e.Pre(middleware.RemoveTrailingSlash())
	e.GET("/status", handlerGetStatus)
	e.POST("/zone", handlerSetZone)
	e.GET("/zone/:zone", handlerGetZone)
	e.DELETE("/zone/:zone", handlerDeleteZone)
	e.GET("/resolve/:name", hanlderResolve)
	e.HideBanner = true
	return e.Start(addr)
}

func handlerGetStatus(c echo.Context) error {
	return c.JSON(200, `I'm here ^^`)
}

func handlerSetZone(c echo.Context) error {
	var zone Zone
	if err := c.Bind(&zone); err != nil {
		return c.JSON(400, map[string]interface{}{
			"success": false,
			"error":   err.Error(),
		})
	}
	if err := DBZoneSet(&zone); err != nil {
		return c.JSON(400, map[string]interface{}{
			"success": false,
			"error":   err.Error(),
		})
	}
	return c.JSON(200, map[string]interface{}{
		"success": true,
		"data":    zone,
	})
}

func handlerGetZone(c echo.Context) error {
	needle := c.Param("zone")
	if needle == "all" {
		zones := []Zone{}
		if err := db.All(&zones); err != nil {
			return c.JSON(404, map[string]interface{}{
				"success": false,
				"error":   err.Error(),
			})
		}
		return c.JSON(200, map[string]interface{}{
			"success": true,
			"data":    zones,
		})
	}
	err, zone := DBZoneGet(needle)
	if err != nil {
		return c.JSON(404, map[string]interface{}{
			"success": false,
			"error":   err.Error(),
		})
	}
	return c.JSON(200, map[string]interface{}{
		"success": true,
		"data":    zone,
	})
}

func handlerDeleteZone(c echo.Context) error {
	DBZoneDelete(c.Param("zone"))
	return c.JSON(200, map[string]interface{}{
		"success": true,
	})
}

func hanlderResolve(c echo.Context) error {
	return c.JSON(200, map[string]interface{}{
		"success": true,
		"data":    DBRecordFind(c.Param("name")),
	})
}
