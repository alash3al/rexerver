<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Home') &raquo; {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.7/css/jquery.dataTables.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/yeti/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
        body {
            padding-top: 70px;
        }
		.dropdown-submenu {
		    position:relative;
		}
		.dropdown-submenu>.dropdown-menu {
		    top:0;
		    left:100%;
		    /*margin-top:-6px;*/
		    margin-left:-1px;
		    /*-webkit-border-radius:0 6px 6px 6px;
		    -moz-border-radius:0 6px 6px 6px;
		    border-radius:0 6px 6px 6px;*/
		}
		.dropdown-submenu:hover>.dropdown-menu {
		    display:block;
		}
		.dropdown-submenu>a:after {
		    display:block;
		    content:" ";
		    float:right;
		    width:0;
		    height:0;
		    border-color:transparent;
		    border-style:solid;
		    border-width:5px 0 5px 5px;
		    border-left-color:#cccccc;
		    margin-top:5px;
		    margin-right:-10px;
		}
		.dropdown-submenu:hover>a:after {
		    border-left-color:#ffffff;
		}
		.dropdown-submenu.pull-left {
		    float:none;
		}
		.dropdown-submenu.pull-left>.dropdown-menu {
		    left:-100%;
		    margin-left:10px;
		    -webkit-border-radius:6px 0 6px 6px;
		    -moz-border-radius:6px 0 6px 6px;
		    border-radius:6px 0 6px 6px;
		}
    </style>
</head>
<body>
    @if(\Auth::check())
        @include('partials.dashboard-nav')
    @endif

    <div class="container">
        @include('partials.alerts')
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.7/js/jquery.dataTables.min.js"></script>
    @yield('footer_scripts')
</body>
</html>
