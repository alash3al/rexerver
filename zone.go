package main

type Record struct {
	Name  string `json:"name"`
	Type  uint16 `json:"type"`
	Value string `json:"value"`
	TTL   int    `json:"ttl"`
}

type Zone struct {
	Name    string   `json:"name" storm:"id"`
	Hits    int64    `json:"hits"`
	Records []Record `json:"records"`
}
