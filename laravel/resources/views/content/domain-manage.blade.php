@extends('layouts.dashboard')

@section('content')

    @php($newIndex = sizeof((array) $domain->records))

    <div class="center-block" style="max-width: 800px">
        <h1 class="text-center">{{strtoupper($domain->name)}} Records</h1>

        <br />

        <form method="post" class="panel panel-default center-block" action="{{route('domain.update', $domain->id)}}">
            {{csrf_field()}}
            {{method_field('put')}}
            <div class="panel-heading">
                <div class="row text-center">
                    <div class="col-md-3"><b>TYPE</b></div>
                    <div class="col-md-3"><b>NAME</b></div>
                    <div class="col-md-2"><b>TTL</b></div>
                    <div class="col-md-3"><b>VALUE</b></div>
                    <div class="col-md-1"><b>Action</b></div>
                </div>
            </div>
            <div class="panel-body" style="padding: 10px">
                @foreach($domain->records as $record)
                @php($id = $record['id'])
                    <div class="row" style="{{$id == 0 ? 'border-left: 10px solid greenyellow' : ''}}" id="record-{{str_slug($id)}}">
                        <input name="records[{{$id}}][id]" value="{{$id}}" type="hidden" />
                        <div class="col-md-3">
                            <select class="form-control" name="records[{{$id}}][type]" required>
                                <option disabled selected>Add New</option>
                                @foreach(\App\Helper\Record::types() as $tid => $text)
                                <option value="{{$tid}}" {{$tid == $record['type'] ? 'selected' : ''}}>{{$text}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="records[{{$id}}][name]" value="{{$record['name']}}" required/>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" type="number" name="records[{{$id}}][ttl]" value="{{$record['ttl'] ?? 3600}}"/>
                        </div>
                        <div class="col-md-3">
                            <input class="form-control" type="text" name="records[{{$id}}][value]" value="{{$record['value'] ?? ''}}" required/>
                        </div>
                        <div class="col-md-1 {{$id === 0 ? 'hidden' : ''}}">
                            <a onclick="if(confirm('Do you really want to remove this record?')) {document.querySelector('#record-{{str_slug($id)}}').remove()} return false" href="?rm={{$record['id']}}" title="Delete"><span class="fa fa-trash fa-2x"></span></a>
                        </div>
                    </div>
                    <br />
                @endforeach
            </div>
            <div class="panel-footer">
                <div class="center-block">
                    <button class="btn btn-success">Save Records</button>
                    <a href="?purge" class="btn btn-danger" onclick="if(!confirm('Are Your sure, you need to delete this domain?!'))return false">Delete {{$domain->name}}</a>
                </div>
            </div>
        </form>

    </div>

@endsection
