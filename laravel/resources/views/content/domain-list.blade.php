@extends('layouts.dashboard')

@section('content')

    <div class="center-block" style="max-width: 600px">
        <h1 class="text-center">Domains Manager</h1>

        <br />

        <form method="post" class="panel panel-default center-block" style="padding: 10px" action="{{route('domain.store')}}">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-9">
                    <input class="form-control" name="name" type="text" placeholder="add new Domain Name, i.e google.com" required/>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-success form-control">Add New</button>
                </div>
            </div>
        </form>

        <br />

        <table class="table table-striped table-hover table-responsive">
            <thead style="font-weight: bold">
                <tr>
                    <!-- <td>#</td> -->
                    <td>Domain</td>
                    <td>Records</td>
                    <td>Actions</td>
                </tr>
            </thead>
            @foreach($domains as $domain)
            <tr>
                <!-- <td>{{$domain->id}}</td> -->
                <td>{{$domain->name}}</td>
                <td>{{sizeof($domain->records)}}</td>
                <td><a href="{{route('domain.edit', $domain->id)}}">Manage</a></td>
            </tr>
            @endforeach
        </table>
    </div>

@endsection
