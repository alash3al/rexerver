<?php

use Illuminate\Http\Request;
use \App\Model\Record;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/resolve/{name}', function($name){
//     $records = Record::with('domain')->ofName($name)->get();
//     $zone =  [
//         'name' => '',
//         'records' => []
//     ];
//
//     foreach ( $records as $record ) {
//         $zone['name'] = $record->name;
//         $zone['records'][] = $record->only(['type', 'name', 'ttl', 'value']);
//     }
//
//     return $zone;
// });
//
// Route::post('/log/{name}', function($name){
//     $domain = Record::with('domain')->ofName($name)->get()->first()->domain;
//     if ( $domain ) {
//         $domain->hits += 1;
//         $domain->save();
//     }
//     return response()->json(true);
// });
