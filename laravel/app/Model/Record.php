<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $fillable = [
        'domain_id',
        'name',
        'type',
        'ttl',
		'value'
    ];

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    public function scopeOfName($query, $name)
    {
        return $query->where('name', $name)->orWhereRaw('? RLIKE CONCAT("%", name)', $name);
    }
}
