<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\User::firstOrCreate(['name' => "admin"], [
            "password" => bcrypt("admin")
        ]);
        \App\Model\User::firstOrCreate(['name' => "admin2"], [
            "password" => bcrypt("admin2")
        ]);
    }
}
