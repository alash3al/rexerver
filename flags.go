package main

import "flag"

var (
	FALLBACK   = flag.String("fallback", "8.8.8.8:53", "the fallback dns server")
	LISTEN_DNS = flag.String("listen-dns", ":53", "the address of dns server to listen on")
	LISTEN_API = flag.String("listen-api", ":80", "the address of the api server to listen on")
	STORAGE    = flag.String("storage", "./rexerver.db", "the data source name")
	CACHE_TTL  = flag.Int64("cache", 600, "the in-memory cache ttl in seconds to save record search time")
	HOSTS      = flag.String("hosts", "/etc/hosts", "hosts file(s) to read from them/it when needed")
)

func InitFlags() {
	flag.Parse()
}
