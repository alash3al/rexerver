<div style="max-width: 700px; margin:auto">
	@if(\Session::has('alert'))
		<div class="alert {{ \Session::get('alert-class', 'alert-info') }}">
			{{ \Session::get('alert') }}
		</div>
	@endif

	@if ($errors->any())
	    <div class="alert alert-danger" style="margin: 15px">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
</div>
