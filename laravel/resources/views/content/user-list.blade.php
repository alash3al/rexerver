@extends('layouts.dashboard')

@section('content')

    <div class="center-block" style="max-width: 600px">
        <h1 class="text-center">System Managers</h1>

        <br />

        <table class="table table-striped table-hover table-responsive">
            <thead style="font-weight: bold">
                <tr>
                    <td>ID</td>
                    <td>Name</td>
                    <td>Actions</td>
                </tr>
            </thead>
            @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td><a href="{{route('user.edit', $user->id)}}">Edit</a></td>
            </tr>
            @endforeach
        </table>
    </div>

@endsection
