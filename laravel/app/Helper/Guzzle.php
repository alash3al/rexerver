<?php namespace App\Helper;

use GuzzleHttp\Client;

class Guzzle
{
	public static function getClient()
	{
		return new Client([
		    'base_uri' => env('API_BASE_URL'),
		]);
	}
}
