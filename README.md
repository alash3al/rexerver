# Rexerver
===========


> A `DNS` MAnager Built from scratch in `Golang` using the microservices design pattern, it consists of `6 services`

* DNS Resolver (with an optional forwarder `fallback`)
* RESTful API (to make it easy to manage the entries)
* Hosts File(s) compatible service, so you can provide `hosts` file to it and it will resolve host names from it.
* Caching layer, to low the `I/O` usage.
* Dashboard using `laravel` (a PHP framework)
* Nginx for service the dashboard.

# Configurations
===============
> There are two sections of configurations,

* `rexerver` shell script
* `nginx configurations`

##### About `rexerver.sh`
```bash
 -cache int
	   the in-memory cache ttl in seconds to save record search time (default 600)
 -fallback string
	   the fallback dns server (default "8.8.8.8:53")
 -hosts string
	   hosts file(s) to read from them/it when needed (default "/etc/hosts")
 -listen-api string
	   the address of the api server to listen on (default ":80")
 -listen-dns string
	   the address of dns server to listen on (default ":53")
 -storage string
	   the data source name (default "./rexerver.db")
```

##### About Nginx
> Its configuration found in `/etc/nginx/sites-enabled/rexerver`

# How To ?
=============

#### $ How to change the dns port ?
> By default any `dns` service uses the `udp 53` port, but you can change it from the shell script `/root/rexerver.sh` by changing the value of `--listen-dns` to your own port.

#### $ How to change the dashboard port ?
> Go to `/etc/nginx/sites-enabled/rexerver`, change the value of `listen` from `8080` to anything else.

#### $ How to change the port of the `RESTful API` ?
> Go to `/root/rexerver.sh`, change the value of `listen-api` to anything else
>  **THEN YOU MUST** change the same configurations in `/var/rexerver/laravel.env`, change `API_BASE_URL` to something else.

#### $ Can I change the cache TTL ?
> Cache is used to make it high performance, so just change the value of `cache` in the `/root/rexerver.sh` to something else.

#### $ How to restart the service/clean the cache ?
> `pm2 restart rexerver`

#### $ What is `/root/go`
> It is a folder for `Golang` a programming language.
