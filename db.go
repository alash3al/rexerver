package main

import "sync"
import "errors"
import "regexp"
import "strings"
import "github.com/asdine/storm"
import "golang.org/x/net/publicsuffix"

var (
	db       *storm.DB
	allZones = map[string]Zone{}
	lock     = &sync.RWMutex{}
)

func DBInit(filename string) (err error) {
	db, err = storm.Open(filename)
	if err != nil {
		return err
	}
	var zones []Zone
	db.All(&zones)
	for _, z := range zones {
		allZones[z.Name] = z
	}
	return nil
}

func DBZoneSet(z *Zone) (err error) {
	var oldZone Zone
	err = db.One("Name", z.Name, &oldZone)
	lock.Lock()
	allZones[z.Name] = *z
	lock.Unlock()
	if err != nil {
		return db.Save(z)
	}
	return db.Update(z)
}

func DBZoneGet(name string) (error, Zone) {
	// err = db.One("Name", name, &z)
	// return err, z
	// lock.RLock()
	// defer lock.Unlock()
	if z, ok := allZones[name]; ok {
		return nil, z
	}
	return errors.New("Not found"), Zone{}
}

func DBZoneDelete(name string) {
	lock.Lock()
	defer lock.Unlock()
	err, zone := DBZoneGet(name)
	if err != nil {
		return
	}
	delete(allZones, name)
	db.DeleteStruct(&zone)
}

func DBRecordFind(name string) []Record {
	records := []Record{}
	err, zone := DBZoneGet(name)

	if err != nil {
		n, _ := publicsuffix.EffectiveTLDPlusOne(name)
		err, zone = DBZoneGet(n)
		if err != nil {
			n, _ = publicsuffix.PublicSuffix(name)
			err, zone = DBZoneGet(n)
		}
	}

	for _, record := range zone.Records {
		if record.Name == "@" {
			record.Name = zone.Name
		} else if record.Name != zone.Name && !strings.HasSuffix(record.Name, "."+zone.Name) {
			record.Name += "." + zone.Name
		}
		if record.Value == "@" {
			record.Value = zone.Name
		}
		record.Name = strings.Replace(record.Name, "*", "(.+)", -1)
		if record.Name == name || regexp.MustCompile(`(?i)^`+record.Name+`$`).MatchString(name) {
			records = append(records, record)
		}
	}

	return records
}
