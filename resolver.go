package main

import "fmt"
import "time"
import "strings"
import "github.com/miekg/dns"
import "github.com/patrickmn/go-cache"

// import "golang.org/x/net/publicsuffix"

// Respolver is our DNS Client & Server,
// that will catch the incoming question, then try
// to find it in our db, if it wasn't found, then we will fallback
// to a global dns server.
type Resolver struct {
	Fallback string
	Addr     string
	gocache  *cache.Cache
}

// Initialize a new resolver "server",
// backend refers to the server we will use as a fallback server,
// addr, is the address we will listen on.
func NewResolverServer(fallback, addr string, ttl time.Duration) error {
	r := &Resolver{
		Fallback: fallback,
		Addr:     addr,
		gocache:  cache.New(time.Duration(ttl), time.Duration(ttl)),
	}
	return r.ListendAndServe()
}

// Listen on the specified addr(udp) and then serve the questions.
func (this Resolver) ListendAndServe() error {
	s := &dns.Server{
		Net:     "udp",
		Addr:    this.Addr,
		Handler: dns.HandlerFunc(this.resolve),
	}
	return s.ListenAndServe()
}

// this will resolve the the incoming question and resolve it.
func (this Resolver) resolve(w dns.ResponseWriter, r *dns.Msg) {
	var msg = new(dns.Msg)
	var err error
	var subMsg *dns.Msg

	defer w.Close()

	for _, question := range r.Question {

		// cache part
		QKey := fmt.Sprintf("%s:%d", question.Name, question.Qtype)
		if cachedMsg, cached := this.gocache.Get(QKey); cached {
			msg.Answer = append(msg.Answer, (cachedMsg.(*dns.Msg)).Answer...)
			continue
		}

		// search in hosts then in db
		records, found := this.find(question.Name, question.Qtype)

		if !found {
			subMsg, err = dns.Exchange(r, this.Fallback)
			if err != nil {
				continue
			}
		} else {
			// our db
			subMsg = new(dns.Msg)
			for _, record := range records {
				rr, _ := dns.NewRR(fmt.Sprintf(
					"%s %d IN %s %s",
					question.Name,
					record.TTL,
					dns.TypeToString[record.Type],
					record.Value,
				))
				fmt.Println(record.Value)
				subMsg.Answer = append(subMsg.Answer, rr)
			}
		}

		if subMsg != nil {
			msg.Answer = append(msg.Answer, subMsg.Answer...)
			this.gocache.Set(QKey, subMsg, cache.DefaultExpiration)
			subMsg = nil
		}
	}

	msg.Compress = true
	msg.RecursionDesired = true
	msg.RecursionAvailable = true

	msg.SetReply(r)
	w.WriteMsg(msg)
}

// find the specified domain in our db.
func (this Resolver) find(name string, qtype uint16) (records []Record, ok bool) {
	name = strings.Trim(name, ".")

	if hostsRecord := HostsFiles.Find(name); hostsRecord != "" {
		return []Record{{
			Name:  name,
			Type:  dns.TypeA,
			TTL:   3600,
			Value: hostsRecord,
		}}, true
	}

	dbRecords := DBRecordFind(name)

	if len(dbRecords) < 1 {
		return nil, false
	}

	for _, record := range dbRecords {
		if qtype == dns.TypeANY || record.Type == qtype {
			records = append(records, record)
		}
	}

	if len(records) > 0 {
		return records, true
	}

	return nil, false
}
