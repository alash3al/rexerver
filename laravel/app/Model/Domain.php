<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Helper\Guzzle;

class Domain extends Model
{
	protected $fillable = [
		'name',
	];

	protected $casts = [
		'records' => 'array'
	];

	public function records()
	{
		return $this->hasMany(Record::class);
	}

	// public function setRecords()
	// {
	//
	// }

	// public function setRecords(array $records)
	// {
	// 	foreach ( $records as &$record ) {
	// 		if ( $record['name'] == '@' ) {
	// 			$record['name'] = $this->name;
	// 		} else if ( ! preg_match("~{$this->name}$~", $record['name']) ) {
	// 			$record['name'] .= '.' . $this->name;
	// 		}
	// 		if ( $record['value'] == '@' ) {
	// 			$record['value'] = $this->name;
	// 		}
	// 		$record['domain_id'] = $this->id;
	// 		if ( empty($record['id']) ) {
	// 			$record = Record::create($record);
	// 		} else {
	// 			$record = Record::find($record['id'])->fill($record)->save();
	// 		}
	// 	}
	// 	return $this;
	// }

	public function getRecordsAttribute() {
		return $this->records()->orderby('name')->get()->prepend([
			'id' => 0,
			'name' => '----',
			'type' => 0,
			'ttl' => 3600,
			'value' => '----'
		])->toArray();
	}
}
