<?php namespace App\Helper;

class Record
{
	public static function types()
	{
		return [
			1	=>	'A',
			2	=>	'NS',
			28 	=>	'AAAA',
			5	=>	'CNAME',
			15	=>	'MX',
			16	=> 	'TXT'
		];
	}
}
