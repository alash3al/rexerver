package main

import "fmt"
import "time"
import "github.com/fatih/color"

func main() {
	// step 1
	colorize(color.FgGreen, "⇛ Parsing the command line flags ...")
	InitFlags()

	// step 2
	colorize(color.FgGreen, "⇛ Loading the specified hosts files ...")
	InitHosts()

	// step 3
	colorize(color.FgGreen, "⇛ Initializing the DB ...")
	if err := DBInit(*STORAGE); err != nil {
		colorize(color.FgRed, err.Error())
		return
	}

	// step 4
	colorize(color.FgGreen, "⇛ API Server started listening on", *LISTEN_API, "...")
	go func() {
		colorize(color.FgRed, APIServerInit(*LISTEN_API))
	}()

	// step 5
	colorize(color.FgGreen, "⇛ DNS Server started listening on", *LISTEN_DNS, "...")
	colorize(color.FgRed, NewResolverServer(*FALLBACK, *LISTEN_DNS, time.Duration(*CACHE_TTL)*time.Second))
}

// colorize the log output
func colorize(c color.Attribute, args ...interface{}) {
	color.Set(c)
	fmt.Println(args...)
	color.Unset()
}
