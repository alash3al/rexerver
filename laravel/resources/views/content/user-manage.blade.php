@extends('layouts.dashboard')

@section('content')

    <div class="center-block" style="max-width: 600px">
        <h1 class="text-center">{{isset($user) ? 'Edit A' : 'Create New'}} Manager</h1>

        <br />

        <form action="{{isset($user) ? route('user.update', $user->id) : route('user.store')}}" method="post" class="panel panel-default" style="padding: 15px">
            {{ method_field(isset($user) ? 'PUT' : 'POST') }}
            {{ csrf_field() }}
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <b>Name: </b>
                            <input placeholder="i.e admin, root, user1" required value="{{@$user->name}}" name="name" class="form-control"/>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>
                            <b>Password: </b>
                            <input placeholder="Password" name="password" class="form-control"/>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <button class="form-control btn btn-success">Save</button>
                    </div>
                    @if(isset($user))
                    <div class="col-md-6">
                        <a class="btn btn-danger form-control" href="{{route('user.index') . '?delete=' . $user->id}}">Delete</a>
                    </div>
                    @endif
                </div>
            </div>
        </form>
    </div>

@endsection
