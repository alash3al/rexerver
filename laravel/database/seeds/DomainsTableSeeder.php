<?php

use Illuminate\Database\Seeder;

class DomainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domains = [
            "google.com",
            "facebook.com",
        ];

        foreach ( $domains as $domain ) {
            \App\Helper\Guzzle::getClient()->post('/zone', ['json' => [
                'name' => $domain,
                'records' =>
                [
                    [
                        'type' => 1,
                        'name' => '@',
                        'ttl' => 3600,
                        'value' => '127.0.0.1'
                    ],
                    [
                        'type' => 1,
                        'name' => '*',
                        'ttl' => 3600,
                        'value' => '127.0.0.1'
                    ]
                ]
            ]]);
        }
    }
}
